#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import re
import gc
import os
import glob
import requests

import logging
from logging import handlers

#import pyhdb
import numpy as np
from io import BytesIO
from flask import Flask, jsonify, request, json, redirect

from ObjDet_image import Recogniser
from ObjDet_main import *
from ObjDet_image import *

app = Flask(__name__)
port = int(os.getenv("PORT", 8041))
directory = 'IMAGES'
# Create object of class Recogniser
rc = Recogniser()

# Error handlers.
# 500 error means troubles on server side
@app.errorhandler(500)
def server_error_c(e):
    tab = ':'*50
    time_stamp = time.asctime()
    app.logger.error('{}\n==={}===\n{}'.format(tab, time_stamp, e))
    return "🔥 Server is on fire 🔥  (stay tuned ಠ⌣ಠ)"

# 400 error means that there is no route
@app.errorhandler(404)
def no_page(e):
    tab = ':'*50
    time_stamp = time.asctime()
    app.logger.error('{}\n=== {} ===\n{}'.format(tab, time_stamp, e))
    #app.logger.error(tab + e + ' | ' + time_stamp + tab)
    return " ಠ_ಠ ...Nothing to see here. Come back later. ಠ_ಠ"
  
# Method returns help
@app.route('/help')
def showHelp():
    help = "🕷️HELP🕸️ \n\tList of routes: \n\t 📝 /log <- method for showing logs \n\t 🤖 /recognition <- method for ticket recognition \n\t 💾 / <- method for loading frozen inference graph in ML model"
    return help

# Show all logs for the all time
@app.route('/log')
def log_listing():
    log_file = glob.glob('*.log')[0]
    with open(log_file, 'r') as file:
        listing = file.read()
    return(listing)

# Starting model. Creating empty directory
@app.route('/', methods = ['GET'])
def check():
    gc.collect()
    os.mkdir(directory)
    rc.startModel()
    return jsonify({'Response' : 'Service is working. Directory successfuly created.'})

# Method that incapsulates methods of class Recogniser
# It appends image URL, rotates it on correct angle, cuts all articles and combines their data in dictionaries
def run(IMAGE_NAME):
    rc.startModel()
    result = cutter(rc.recognition(angle_horizen(IMAGE_NAME)), angle_horizen(IMAGE_NAME))
    return result

# Method that is not implemented yet
def saveInHANA():
    pass

# Complex method that appends image and return dictionaries with recognised text data
@app.route('/recognition' , methods = ['POST', 'PUT'])
def recognition():
    #return(jsonify({'Directories':str(directory in os.listdir('./'))}))
    #return(jsonify({'Directories':str(os.listdir('./'))}))
    if request.method == 'POST' and directory in os.listdir('./'):
        # image: image.jpg
        # Get data by its key
        file = request.files['image']
        # File path
        file_path = './'+directory+'/image'


        # Check is image inside directory or not
        #print(os.listdir('./'+ directory))


        # Save data to directory
        file.save(file_path)
        #Run recogniser
        #rc.recognition('image')
        result = run(file_path)

        # Colect garbage
        gc.collect()

        # Posting data to DB in schema PRICE_TEXT_BARCODE
        postData(result)

        # Return the response of recognised prices
        return(jsonify({'Image is loaded.': 'True', 'Recognition result': result}))
    else:
        return(jsonify({'Image is loaded.': 'False', 'Error': 'Method POST/PUT is available. Please change your method type. Or go to / with GET to create folder for images.'}))

# Method for posting data inside HANA DB
def postData(data_to_post = {"No data":{"2":"no data", "3":"no data"}}):
    url = "https://coslevisgd_tradeforum.cfapps.eu10.hana.ondemand.com/api/v2.0/db"
    headers = {'Content-Type':'application/json'}
    
    # We iterate through all keys in dictionary
    for element in data_to_post.keys():
        print(data_to_post[element])
        
        # Check whether is price value in dictionary
        try:
            defaultPrice = 50
            if 2 in data_to_post[element]:
                price = data_to_post[element][2]
                print('\nPrice: ', price, '\n')
                if len(price) == 0:
                    price = defaultPrice
                else:
                    price = int(re.findall('\d+',price)[0])
                    print(price)
            else:
                price = defaultPrice
        except Exception as e:
            #print(e)
            price = defaultPrice
        # Check whether is name value in dictionary
        try:
            if 3 in data_to_post[element]:
                name = data_to_post[element][3]
                if len(name) == 0:
                    name = "Oooops. Cannot recognise."
                else:
                    name = str(name)
            else:
                name = str(name)
        except:# KeyError as ke:
            name = "Oooops. Cannot recognise."

        payload = "{\"host\" : \"10.253.181.171\", \"port\" : \"30041\", \"user\" : \"FACEMETRIC\", \"password\" : \"Qwerty12345\", \"schema\" : \"16148E618265420793B89D0C48052EBC\", \"data\" : \"{'price':'%s', 'text':'%s', 'barcode':'80085'}\"}" % (price, name)
        response = requests.request("POST", url, data=payload.encode('utf-8'), headers=headers)

        print('\n\n* * *\n', response.text, '\n* * *\n\n')

    return "Data posted successfully"

if __name__ == '__main__': 
    # Run app
    app.run(host = '0.0.0.0', port = port, ssl_context = ('./cert.pem','./key.pem'))
    #app.run(host = '0.0.0.0', port = port, debug = True)
