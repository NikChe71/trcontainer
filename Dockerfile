# start from base
FROM ubuntu:14.04
MAINTAINER Cherenev Nikita <nikita.cherenev@sap.com>

# Update repositories
RUN zypper ar http://download.opensuse.org/distribution/leap/15.0/repo/oss/ oss

# copy our application code
RUN cd /
RUN mkdir ./RECOG_SERVICE
RUN cd ./RECOG_SERVICE
WORKDIR ./RECOG_SERVICE
RUN git clone git clone https://NikChe71@bitbucket.org/NikChe71/ticketrecognitionservice.git

# install system-wide deps for python
RUN zypper install python3
RUN pip3 install -r requirements.txt
RUN zypper install tesseract-ocr

# expose port
EXPOSE 5000

# start app
CMD [ "python3.6", "./run.py" ]
