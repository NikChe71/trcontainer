import os
import unittest
from stencil import StencilImage

class TestStencilMethods(unittest.TestCase):

    def test_num_fields(self):
        self.assertEqual(len(StencilImage().stencil_crop('./example.png', 'X5')), 3)

    def test_image_extensions(self):
        images = os.listdir('.\\test\\test_images')
        results = []
        for image in images:
            result = StencilImage().stencil_crop('./example.png', 'X5')
            results.append(result)
        self.assertGreaterEqual(len(results), 3)
    

if __name__ == '__main__':
    unittest.main()