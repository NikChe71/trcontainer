import cv2
import os
import sys
import time
import numpy as np
from PIL import Image
import tensorflow as tf

class Recogniser():
    def __init__(self):
        self.CWD_PATH = './'
        MODEL_NAME = 'frozen_inference_graph.pb'
        self.PATH_TO_LABELS = os.path.join(self.CWD_PATH,'training','labelmap.pbtxt')
        # Path to frozen detection graph .pb file, which contains the model that is used
        # for object detection.
        self.PATH_TO_CKPT = self.CWD_PATH + MODEL_NAME
        NUM_CLASSES = 4

    def startModel(self):
        # Load the Tensorflow model into memory.
        detection_graph = tf.Graph()
        with detection_graph.as_default():
            od_graph_def = tf.GraphDef()
            with tf.gfile.GFile(self.PATH_TO_CKPT, 'rb') as fid:
                serialized_graph = fid.read()
                od_graph_def.ParseFromString(serialized_graph)
                tf.import_graph_def(od_graph_def, name='')
        
            self.sess = tf.Session(graph=detection_graph)
        
        self.image_tensor = detection_graph.get_tensor_by_name('image_tensor:0')
        self.detection_boxes = detection_graph.get_tensor_by_name('detection_boxes:0')
        self.detection_scores = detection_graph.get_tensor_by_name('detection_scores:0')
        self.detection_classes = detection_graph.get_tensor_by_name('detection_classes:0')
        self.num_detections = detection_graph.get_tensor_by_name('num_detections:0')

    # ----------------------------------------------------------------------------    

    def coord_of_box(self, list_of_coord):
        coord = [int(list_of_coord[0]*self.width), int(list_of_coord[1]*self.height), int(list_of_coord[2]*self.width), int(list_of_coord[3]*self.height)]
        return coord

    def centre_of_box(self, list_of_coord):
        centre = [int(np.mean([(list_of_coord[1]*self.height), (list_of_coord[3]*self.height)])), int(np.mean([(list_of_coord[0]*self.width), (list_of_coord[2]*self.width)]))]
        return centre
    
    def dot_in_box(self, coor_dot, coor_box):
        if coor_box[0] < coor_dot[1] < coor_box[2] and coor_box[1] < coor_dot[0] < coor_box[3]:
            return True
    
    def recognition(self, image_name):
        tickets = []
        prices = []
        names = []
        barcodes = []
        
        centre_prices = []
        centre_names = []
        centre_barcodes = []
#        self.PATH_TO_IMAGE = self.CWD_PATH + image_name
#        image = Image.open(self.PATH_TO_IMAGE)
        image = image_name
        image_expanded = np.expand_dims(image, axis=0)
        
        # Perform the actual detection by running the model with the image as input
        (boxes, scores, classes, num) = self.sess.run(
            [self.detection_boxes, self.detection_scores, self.detection_classes, self.num_detections],
            feed_dict={self.image_tensor: image_expanded})

        
        self.height = np.shape(image)[1]
        self.width = np.shape(image)[0]
        
        for box in enumerate(np.squeeze(boxes)):
            if(np.squeeze(classes)[box[0]] == 3):
                if(np.squeeze(scores)[box[0]] > 0.5):
                    box[1][0] = box[1][0]*0.94
                    box[1][1] = box[1][1]*0.9
                    box[1][2] = box[1][2]*1.01
                    box[1][3] = box[1][3]*1.04   
        cord_list = []
        for box, clas in zip(enumerate(np.squeeze(boxes)), enumerate(np.squeeze(classes))):
            if(np.squeeze(scores)[box[0]] > 0.5):
                cord_list.append([clas[1], box[1][0]*self.width,box[1][1]*self.height,box[1][2]*self.width,box[1][3]*self.height])
                if clas[1] == 1:
                    tickets.append(self.coord_of_box(box[1]))
                if clas[1] == 2:
                    prices.append(self.coord_of_box(box[1]))
                    centre_prices.append(self.centre_of_box(box[1]))
                    cv2.circle(image, tuple(self.centre_of_box(box[1])), 5, color = (0, 255, 255), thickness = -1)
                    cv2.putText(image, str(self.centre_of_box(box[1])), tuple(self.centre_of_box(box[1])), cv2.FONT_HERSHEY_PLAIN , 1,(255,255,255),1,cv2.LINE_AA)
                if clas[1] == 3:
                    names.append(self.coord_of_box(box[1]))
                    centre_names.append(self.centre_of_box(box[1]))
                    cv2.circle(image, tuple(self.centre_of_box(box[1])), 5, color = (47, 255, 173), thickness = -1)
                    cv2.putText(image, str(self.centre_of_box(box[1])), tuple(self.centre_of_box(box[1])), cv2.FONT_HERSHEY_PLAIN , 1,(255,255,255),1,cv2.LINE_AA)
                if clas[1] == 4:
                    barcodes.append(self.coord_of_box(box[1]))
                    centre_barcodes.append(self.centre_of_box(box[1]))
                    cv2.circle(image, tuple(self.centre_of_box(box[1])), 5, color = (255, 255, 255), thickness = -1)    
                    cv2.putText(image, str(self.centre_of_box(box[1])), tuple(self.centre_of_box(box[1])), cv2.FONT_HERSHEY_PLAIN , 1,(255,255,255),1,cv2.LINE_AA)     

#                print(([clas[1], box[1][0]*width,box[1][1]*height,box[1][2]*width,box[1][3]*height]))        
#        print(cord_list)

        full_d = {}
        d = {}
        coord = []
        for i in enumerate(tickets):
            coord = []
            d_d = {}
            for p in enumerate(centre_prices):
                if self.dot_in_box(p[1], i[1]):
                    d_d[2] = prices[p[0]]
                    coord.append([2, prices[p[0]]])
                else: 
                    pass
            for n in enumerate(centre_names):
                if self.dot_in_box(n[1], i[1]):
                    d_d[3] = names[n[0]]
                    coord.append([3, names[n[0]]])
                else: 
                    pass
            for b in enumerate(centre_barcodes):
                if self.dot_in_box(b[1], i[1]):
                    d_d[4] = barcodes[b[0]]
                    coord.append([4, barcodes[b[0]]])
                else: 
                    pass
            d[i[0]] = coord 
            full_d[i[0]] = d_d
            
        return full_d
    
    def bounding(self):
        image = cv2.imread(self.PATH_TO_IMAGE)
        label_map = label_map_util.load_labelmap(self.PATH_TO_LABELS)
        categories = label_map_util.convert_label_map_to_categories(label_map, max_num_classes=self.NUM_CLASSES, use_display_name=True)
        category_index = label_map_util.create_category_index(categories)
        
        # Draw the results of the detection (aka 'visulaize the results')
        vis_util.visualize_boxes_and_labels_on_image_array(
            image,
            np.squeeze(boxes),
            np.squeeze(classes).astype(np.int32),
            np.squeeze(scores),
            category_index,
            use_normalized_coordinates=True,
            line_thickness=5,
            min_score_thresh=0.50)

        # All the results have been drawn on image. Now display the image.
        cv2.imshow('Object detector', image)
        
        
