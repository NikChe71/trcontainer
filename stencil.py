import cv2
import ast
import numpy as np

# Load percentages out from config file
def load_stencil():
    print("Choose retailer:\nX5 | Perekrestok | Pyaterochka")
    file = open('./templates/config', 'rt')
    retail_price_stencil = ast.literal_eval(file.read())
    return retail_price_stencil

# Translate percentages to pixels
def percent_to_pixel(stencil_template, parameter, image_x, image_y):
    # Types of coordinates
    nameCoordinates = {'x0':0, 'y0':0, 'x1':0, 'y1':0}
    priceCoordinates = {'x0':0, 'y0':0, 'x1':0, 'y1':0}
    barcodeCoordinates = {'x0':0, 'y0':0, 'x1':0, 'y1':0}
    # Combine all coordinates in one list
    coordinates = [nameCoordinates, priceCoordinates, barcodeCoordinates]
    # List for switching dimension to write process
    dimension = [image_y, image_x, image_y, image_x]
    # Iterate through each type of fields (name, price, barcode) and stencil template fields in the same time
    for coordinateType, stencilElement in zip(coordinates, stencil_template[parameter]):
        # Iterate through each coordinate in both files and add a "dimension" parameter to correspond values to right dimensions 
        for stencilCoordinate, coordinate, dimensionParameter in zip(stencil_template[parameter][stencilElement], coordinateType.keys(), dimension):
            #print('stencilCoordinate: ', stencilCoordinate)
            coordinateType[coordinate] = stencilCoordinate*dimensionParameter/100
        #print('coordinateType: ', coordinateType)
    #print('coordinates: ', coordinates)
    return coordinates

# Cropp image parts in choosen percentages
def stencil_crop(image_link, retailer_name):
    # Reading image
    image = cv2.imread(image_link)
    # Show loaded image
    #cv2.imshow('image', image)
    #cv2.waitKey(0)
    #cv2.destroyAllWindows()
    # Getting image shape
    print(np.shape(image))
    x, y, z = np.shape(image)
    # Loading sample out from config file
    stencil = load_stencil()
    # Pass all coordinates to method that translates it to pixels
    allCoordinates = percent_to_pixel(stencil, 'X5', x, y)
    # Create empty place for specific parts of images
    FIELDS = {'name' : [], 'price' : [], 'barcode' : []}
    # Iterate through all logic fields like name, price and barcode
    for field, part in zip(allCoordinates, FIELDS):
        # Pass arguments from template to cropping parameters
        x0 = int(field['x0'])
        y0 = int(field['y0'])
        x1 = int(field['x1'])
        y1 = int(field['y1'])
        print('Y: ', y0, y1, 'X: ', x0, x1)
        # Cropping images in choosen percentages
        cropp = image[y0:y1, x0:x1] # Example: image[y:y+h, x:x+w]
        # Appending cropp image to dictionary for posting all data to tesseract
        FIELDS[part] = cropp
        # Showing sropped image
        #cv2.imshow("Cropped image", cropp)
        # Garbage collector of opencv
        #cv2.waitKey(0)
    # Remove all open windows from desktop
    #cv2.destroyAllWindows()
    return FIELDS

#Example of using this function
#result = stencil_crop('./example.png', 'X5')
#print(result)
