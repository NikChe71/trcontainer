# -*- coding: utf-8 -*-
from __future__ import division, print_function
import re
import os
import cv2
import math
import pytesseract
import numpy as np
import pyzbar.pyzbar as pyzbar
from scipy import ndimage
from PIL import Image
from pytesseract import image_to_string

pytesseract.pytesseract.tesseract_cmd = './tesseract'
tikets = open("tikets.txt", "r", encoding="ISO-8859-1").read().splitlines() # encode ticket values

def angle_horizen(img_path):
    img_before = cv2.imread(img_path)
#    cv2.imshow("Before", img_before)    
    key = cv2.waitKey(0)
    
    img_gray = cv2.cvtColor(img_before, cv2.COLOR_BGR2GRAY)
    img_edges = cv2.Canny(img_gray, 100, 100, apertureSize=3)
    lines = cv2.HoughLinesP(img_edges, 1, math.pi / 180.0, 100, minLineLength=100, maxLineGap=5)
    angles = []
    
    for x1, y1, x2, y2 in lines[0]:
        cv2.line(img_before, (x1, y1), (x2, y2), (255, 0, 0), 3)
        angle = math.degrees(math.atan2(y2 - y1, x2 - x1))
        angles.append(angle)
    
    median_angle = np.median(angles)
    img_rotated = ndimage.rotate(img_before, median_angle)
    
#    print("Angle is {}".format(median_angle))
#    cv2.imwrite('rotated.jpg', img_rotated)
    return img_rotated

def clear_dir():
    if len(os.listdir("cutter_img")) > 0:
#        print(len(os.listdir("cutter_img")))
        print("dir is not clear")
        print("clearing")
    for i in  os.listdir("cutter_img"):
        os.remove("cutter_img/" + i)
    print("all clear", "\n")

def cutter(cord_list, img_name):
    full_dic = cord_list
#    print(cord_list)
#    img = Image.open('./IMAGES/' + img_name)
    img = Image.fromarray(img_name)
    
    for i in cord_list.keys():
#        print(cord_list[i])
        for j in cord_list[i]:
#            print(j, cord_list[i][j])
            bill = i
            lable = (j)
            Ymin = (cord_list[i][j][0])
            Xmin = (cord_list[i][j][1])
            Ymax = (cord_list[i][j][2])
            Xmax = (cord_list[i][j][3])        
            narray = [lable, Xmin, Ymin, Xmax, Ymax] 
            area = (narray[1], narray[2], narray[3], narray[4])
            cropped_img = img.crop(area)
            if j == 2:
#                print(decode_text_new(cropped_img))
                full_dic[i][j] = decode_text_new(cropped_img)
            elif j == 3:
#                print(decode_text_new(cropped_img))
                full_dic[i][j] = decode_text_new(cropped_img)
#                print(full_dic[i][j])
            elif j == 4:
                decode_barcode_new(cropped_img)
#    cv2.imshow('Object detector', img_name)
    #print(full_dic)
    return full_dic
    '''    
    for j in cord_list:
        lable = math.ceil(float(j[0]))
        Ymin = math.ceil(float(j[1]))
        Xmin = math.ceil(float(j[2]))
        Ymax = math.ceil(float(j[3]))
        Xmax = math.ceil(float(j[4]))
        narray = [lable, Xmin, Ymin, Xmax, Ymax] 
        area = (narray[1], narray[2], narray[3], narray[4])
        cropped_img = img.crop(area)

        if narray[0] == 2:
            decode_text_new(cropped_img)
        elif narray[0] == 3:
            decode_text_new(cropped_img)
        elif narray[0] == 4:
            decode_barcode_new(cropped_img)
    '''
def decode_barcode_new(img_file):
#    image = Image.open(img_file).convert('RGB') 
    decodedObjects = pyzbar.decode(img_file)    
    do = [obj for obj in decodedObjects]
    for barcode in decodedObjects:
        rect = barcode.rect
        poly = barcode.polygon
        type = barcode.type
        data = barcode.data
        
#        print('rect : ', rect)
#        print('poly : ', poly)
#        print('Type : ', type)
#        print('Data : ', data,'\n') 
        
        #print(img_file)
        #print('Type : ', obj.type)
        #print('Data : ', obj.data,'\n')  

    return do
        
    
def decode_text_new(img_file):
    text = image_to_string(img_file, lang='rus+eng')
    text = re.sub('[\n\r"]', ' ', text)
    
    dist = []
    for i in tikets:
        dist.append(distance(text, i))
#        print(distance(text, i))
        
    all = zip(dist, tikets)
    index_dist = dist.index(min(dist))
    if min(dist) < 25:
        new_text = tikets[index_dist]
    else:
        new_text = text
#    print(new_text)
    
    return new_text


def distance(a, b):
    a = a.upper()
    b = b.upper()
    "Calculates the Levenshtein distance between a and b."
    n, m = len(a), len(b)
    if n > m:
        # Make sure n <= m, to use O(min(n,m)) space
        a, b = b, a
        n, m = m, n

    current_row = range(n+1) # Keep current and previous row, not entire matrix
    for i in range(1, m+1):
        previous_row, current_row = current_row, [i]+[0]*n
        for j in range(1,n+1):
            add, delete, change = previous_row[j]+1, current_row[j-1]+1, previous_row[j-1]
            if a[j-1] != b[i-1]:
                change += 1
            current_row[j] = min(add, delete, change)

    return current_row[n]
